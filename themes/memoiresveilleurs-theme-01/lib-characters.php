<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/characters";
$search = glob("".$folder."/*.json");
echo '<figure class="wiki-figure">';
echo '<a href="/data/medias/screenshots/pers.png"><img src="/data/medias/screenshots/pers.tb.png" class="wiki-img" alt="Schema"/></a>';
echo '<figcaption class="wiki-caption">Schema</figcaption>';
echo '</figure>';
/*echo '<div class="flex-mv">';
echo '<p>Interviews</p>';
echo '<ul class="no-style-liste">';
echo '<li><a href="';
$plxShow->racine();
echo $lang.'/article3/interview-1-jack-cooper">Interview n°1 Jack Cooper</a></li>';
echo '<li><a href="';
$plxShow->racine();
echo $lang.'/article5/interview-n2-alexandre-bridger">Interview n°2 Alexandre Bridger</a></li>';
echo '<li><a href="';
$plxShow->racine();
echo $lang.'/article6/interview-n3-solene-andersen-cooper">Interview n°3 Solène Andersen Cooper</a></li>';
echo '<li><a href="';
$plxShow->racine();
echo $lang.'/article7/interview-n4-jack-cooper">Interview n°4 Jack Cooper</a></li>';
echo '</ul>';
echo '</div>';*/
echo '<div><a href="/'.$lang.'/static11/charactersinterview">';
echo $plxShow->lang('INTERVIEWTITLE');
echo '</a>';

if (!empty($search)){ 
    foreach ($search as $jsonpath) {
        $iditem = basename($jsonpath,'.json');
        $contents = file_get_contents($jsonpath);

        $get = json_decode($contents); 
        echo '<div class="wiki-item" id="char-'.$iditem.'">';
        echo '<h3 class="wiki-title-item">'.$get->{'name'}.'</h3>';  
        if ($get->{'img'} !== "" ){  
            echo '<figure class="wiki-figure">';
            $alt = $get->{'imgalt'}->{$lang};
            
            echo '<img src="'.$get->{'img'}.'" class="wiki-img" alt="'.$alt.'"/>';
           
            echo '<figcaption class="wiki-caption">'.$alt.'</figcaption>';
            echo '</figure>';
        }
        $soundtheme = $get->{'media'}->{'sound'}->{'theme'};
        $desc = $get->{'desc'}->{$lang};
        $people = $get->{'people'}->{$lang};
        $planetorigin = $get->{'planetorigin'}->{'name'}->{$lang};
       
        if ( $lang =='fr' ){
            $agewhendied = $get->{'agewhendied'}.' ans';
            $birthdate = $get->{'birthdate'}->{'day'}.'/'.$get->{'birthdate'}->{'month'}.'/'.$get->{'birthdate'}->{'year'};
            $deathdate = $get->{'deathdate'}->{'day'}.'/'.$get->{'deathdate'}->{'month'}.'/'.$get->{'deathdate'}->{'year'};
        } elseif ($lang == 'en'){
            $agewhendied = $get->{'agewhendied'}.' years old';
            $birthdate = $get->{'birthdate'}->{'month'}.'/'.$get->{'birthdate'}->{'day'}.'/'.$get->{'birthdate'}->{'year'};
            $deathdate = $get->{'deathdate'}->{'month'}.'/'.$get->{'deathdate'}->{'day'}.'/'.$get->{'deathdate'}->{'year'};
        }        
        echo '<p>'.$desc.'</p>';
        # music theme
        if($get->{'media'}->{'sound'}->{'theme'} !== ""){
            if($soundtheme->{'type'} !== "none"){
                echo '<p><b>';
                echo $plxShow->lang('SOUND_THEME');
                echo '</b></p>';
            }
            if($soundtheme->{'type'} === "bandcamp"){
                echo '<iframe style="border: 0; width: 100%; height: 42px;" src="'.$soundtheme->{'urlsound'} .' seamless><a href="'.$soundtheme->{'urlalbum'}.'">'.$soundtheme->{'title'} .'</a></iframe>';
            }
        }
        echo '<div class="wiki-item-detail"><dl>';
        echo '<dt class="wiki-dt">';
        echo $plxShow->lang('PLANET_BIRTH');
        echo '</dt>';
        echo '<dd class="wiki-dd">'.$planetorigin.'</dd>';
        echo '<dt class="wiki-dt">';
        echo $plxShow->lang('PEOPLE');
        echo '</dt>';
        echo '<dd class="wiki-dd">'.$people.'</dd>';
       
        if($birthdate!=="//"){
            echo '<dt class="wiki-dt">';
            echo $plxShow->lang('DATE_BIRTH');
            echo '</dt>';
            echo '<dd class="wiki-dd">'.$birthdate.'</dd>';           
        }
        if($deathdate!=="//"){
            echo '<dt class="wiki-dt">';
            echo $plxShow->lang('DATE_DEATH');
            echo '</dt>';
            echo '<dd class="wiki-dd">'.$deathdate.'</dd>';           
        }
        if($get->{'agewhendied'}!==""){
            echo '<dt class="wiki-dt">';
            echo $plxShow->lang('AGEDIED');
            echo '</dt>';
            echo '<dd class="wiki-dd">'.$agewhendied.'</dd>';
        }
        echo '</dl></div></div>';
        echo '<br/><div class="separation"></div><br/>';
    } 
} 
?>