<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/publis/";

if($detailjson === 'no'){
    $jsonpath = $folder.'list-publi.json';
    $contents = file_get_contents($jsonpath);
    $get = json_decode($contents); 
    $publis = $get->{'publis'};
    asort($publis);
    echo '<header><h2>';
	$plxShow->staticTitle();
    echo '</h2></header>';
    
    foreach($publis as $pub){
        if($pub->{$typepubli}==="1"){
            $jsondetail = $pub->{'urijson'};
            echo '<div class="" id="char-'.$jsondetail.'">';
            echo '<h3 class="wiki-title-item">'.$pub->{'name'}->{$lang}.'</h3>';  
            if ($pub->{'img'} !== "" ){  
                echo '<figure class="">';
                $alt = $pub->{'imgalt'}->{$lang};
                echo '<img src="/data/medias/couvertures/'.$pub->{'img'}.'" class="" alt="'.$alt.'" width="200px"/>';
                echo '<figcaption class="">';
                echo '<a href="/'.$lang.'/static14/publi?novel='.$jsondetail.'&type='.$typepubli.'">';
                $plxShow->lang('DETAILED_PAGE');
                echo '</a>';
                echo '</figcaption>';
                echo '</figure>';
            }
        }
    }
} else {
    $jsondetail = $_GET["novel"];
    $typepubli =  $_GET["type"];
   // echo 'page de detail'.$jsondetail;
    $pagedetailjson = $folder.$jsondetail.'.json';
   // echo $pagedetailjson;
   // echo $typepubli;
    $contentDetailed = file_get_contents($pagedetailjson);
    $detail = json_decode($contentDetailed);
    echo '<h3 class="wiki-title-item">'.$detail->{'name'}->{$lang}.'</h3>';
   if($lang==="en"){
       if ($detail->{'translate'}==="0"){ //rien n'est traduit
           echo '<h2>This novel is only available in french.</h2>';
           $langtmp = "fr";
           $langtmpfreeread = "fr";
       }else if($detail->{'translate'}==="1"){//la version publiee est traduite
           $langtmp=$lang;
           $langtmpfreeread = "fr";
       } else if($detail->{'translate'}==="2"){ // la version en lecture libre est traduite
           $langtmp= "fr";
           $langtmpfreeread = $lang;
       } else{ // les deux versions sont traduites
           $langtmp= $lang;
           $langtmpfreeread = $lang;
       }
   } else {
    $langtmp = $lang;
    $langtmpfreeread = $lang;
   }
   $thumbimgcover = 'data/medias/couvertures/'.$detail->{'img'};

   
   if($typepubli==="official"){
    echo '<div class="col-3">';
   echo '<figure class="flex-mv">';
   echo'<a href="'.$thumbimgcover.'">';
   echo'<img class="figure-img-publi" src="'.$thumbimgcover.'" alt="'.$detail->{'imgalt'}->{$langtmp}.'" title="'.$detail->{'imgcaption'}->{$langtmp}.'">';
   echo'</a>';
   echo'<figcaption class="figure-caption-publi"><blockquote>'.$detail->{'chapo'}->{$langtmp}.'</blockquote></figcaption>';
   echo'</figure>';
   echo '</div>';
       if($detail->{'trailer'}->{'url'}!==""){
            echo '<iframe src="'.$detail->{'trailer'}->{'url'}.'" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ';
            echo 'allowfullscreen="" width="650" height="366" frameborder="0"></iframe>';
        } 
        echo ' <div class="cyan-bluish-gray-background-color"> <h4>';
        $plxShow->lang('RESUME');
        echo '</h4>';
        echo $detail->{'resume'}->{$langtmp};
        echo '</div>';
        echo ' <div class="cyan-bluish-gray-background-color"> <h4>';
        $plxShow->lang('COVER');
        echo '</h4>';
        echo $detail->{'cov4'}->{$langtmp};
        echo '</div>';
        echo '<br/><div class="separation"></div><br/>';

        $extracts = $detail->{'extracts'};
        if(!empty($extracts)){ 
            echo '<div class="light-gray-background-color">';     
            if(count($extracts)>1){
                $severalext = "true";
                $countext = count($extracts);
            } else{
                $severalext = "false";
            }
            foreach($extracts as $extract){
                $cssextract = "extract-publi-single";
                $plxShow->lang('EXTRACT');
                if($severalext==="true"){
                    $idext = $extract->{'id'};
                    echo ' n°'.$idext;
                    if($countext!==intval($idext)){
                        $cssextract = "extract-publi";
                    } else {
                        $cssextract = "extract-publi-single";
                    }
                }
                $pagehtmldisplay = $folder."content/".$extract->{'textrefpage'}->{$langtmp}.".html";
                // /echo $pagehtmldisplay;
                $data = file_get_contents($pagehtmldisplay);
                echo '<div class="'.$cssextract.'">';
                $txt = html_entity_decode($data);
                echo $txt;
                echo '</div>';
            }   
         echo '</div>';
            echo '<br/><div class="separation"></div><br/>';            
        }
        if($detail->{'other'}->{$langtmp}!==""){
            echo '<p>'.$detail->{'other'}->{$langtmp}.'</p>';
        }
        $othersimgs=$detail->{'otherimg'};        
        if(!empty($othersimgs)){
            foreach($othersimgs as $otherimgcover){
                $thumbotherimgcover = 'data/medias/'.$otherimgcover->{"oimg"};
                echo '<div class="col-3">';
                echo '<figure class="flex-mv">';
                echo'<a href="'.$thumbotherimgcover.'">';
                echo'<img class="figure-img-publi" src="'.$thumbotherimgcover.'" alt="'.$otherimgcover->{'oalt'}->{$langtmp}.'" title="'.$otherimgcover->{'oalt'}->{$langtmp}.'">';
                echo'</a>';
                echo'<figcaption class="figure-caption-publi"><blockquote>'.$otherimgcover->{'odesc'}->{$langtmp}.'</blockquote></figcaption>';
                echo'</figure>';
                echo '</div>';
            }
            echo '<br/><div class="separation"></div><br/>';  
        }
        echo '<p>';
        $plxShow->lang('EDITOR');
        echo ' '.$detail->{'published'}->{'editor'}.', ';
        $plxShow->lang('PUBLISH');
        $datepublish = $detail->{'published'}->{'datepublication'}->{'month'}.'/'.$detail->{'published'}->{'datepublication'}->{'year'};
        echo ' '.$datepublish.'</p>';
        if(!empty($detail->{'urls'}->{'paper'})){
            echo '<p>';
            $plxShow->lang('PAPER');
            echo ' <a href="'.$detail->{'urls'}->{'paper'}->{$langtmp}.'" target="blank" rel="noopener, noreferrer">'.$detail->{'urls'}->{'paper'}->{$langtmp}.'</a>';
            echo ' (ISBN : '.$detail->{'isbn'}->{'paper'}->{$langtmp}.')</p>';
            echo '<p>';
        }
        $plxShow->lang('EBOOK');
        echo ' <a href="'.$detail->{'urls'}->{'ebook'}->{$langtmp}.'" target="blank" rel="noopener, noreferrer">'.$detail->{'urls'}->{'ebook'}->{$langtmp}.'</a>';
        echo ' (ISBN : '.$detail->{'isbn'}->{'ebook'}->{$langtmp}.')</p>';
        if($detail->{'urls'}->{'freereadext'}->{$langtmp}){
            echo '<p>';
            
            echo ' <a href="/'.$lang.'/static14/publi?novel='.$jsondetail.'&type=freeread">';
            $plxShow->lang('FREEREAD');
            echo '</a>';
            echo '</p>';
        }
    } else {
       // echo $langtmpfreeread;
       echo '<div class="col-3">';
       echo '<figure class="flex-mv">';
       echo'<a href="'.$thumbimgcover.'">';
       echo'<img class="figure-img-publi" src="'.$thumbimgcover.'" alt="'.$detail->{'imgalt'}->{$langtmp}.'" title="'.$detail->{'imgcaption'}->{$langtmp}.'">';
       echo'</a>';
       echo'<figcaption class="figure-caption-publi"><blockquote>'.$detail->{'chapo'}->{$langtmpfreeread}.'</blockquote></figcaption>';
       echo'</figure>';
       echo '</div>';
            $urlext = $detail->{'urls'}->{'freereadext'}->{$langtmpfreeread};
      //  echo $urlext;
        $num = pathinfo($urlext);
       // echo $num['basename'];
        echo '<div style="text-align: center;">';
        echo '<iframe src="https://www.atramenta.net/embed/'.$num['basename'].'" style="border: 1px solid #978272;" width="550" height="550">';
        echo '<a href="'.$urlext.'" style="border: 1px solid #978272;" width="550" height="550">';
        echo $detail->{'name'}->{$langtmpfreeread}.', de Louise Morens</a></iframe></div>';
    
        if($detail->{'translate'}==="2"){
            if($lang === "fr"){
                $langswitch = "en";
            } else{
                $langswitch = "fr";
            }
            echo ' <a href="/'.$langswitch.'/static14/publi?novel='.$jsondetail.'&type=freeread">';
            $plxShow->lang('SWITCHLANG');
            echo '</a>';
        }
    }


} 

    
?>