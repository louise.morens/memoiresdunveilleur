<?php if (!defined('PLX_ROOT')) exit; ?>

<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="apple-touch-icon" href="<?php $plxShow->template(); ?>/img/favicon.png">
  	<link rel="apple-touch-icon" sizes="72x72" href="<?php $plxShow->template(); ?>/img/favicon.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="<?php $plxShow->template(); ?>/img/favicon.png">
  	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/plucss.css" media="screen"/>
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/theme.css" media="screen"/>
	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
</head>

<body id="top" class="page mode-<?php $plxShow->mode(true) ?>">

	<header class="header sticky">

		<div class="container">

			<div class="grid">

				<div class="col site-name sml-6">

					<div class="logo">
					
						<h1 class="no-margin heading-small"><?php $plxShow->mainTitle('link'); ?></h1>
						<h2 class="h2 no-margin author"><?php $plxShow->subTitle(); ?></h2>
					
					</div>

				</div>

				<div class="col sml-6">
					<div id="langs">
						<a href="<?php $plxShow->racine() ?>en/"><img src="<?php $plxShow->template(); ?>/img/en.png" alt="<?php $plxShow->lang('LANG_EN') ?>"/></a>
						<a href="<?php $plxShow->racine() ?>fr/"><img src="<?php $plxShow->template(); ?>/img/fr.png" alt="<?php $plxShow->lang('LANG_FR') ?>"/></a>
					</div>
					<nav class="nav">

						<div class="responsive-menu">
							<label for="menu"></label>
							<input type="checkbox" id="menu">
							<ul class="menu">
								<?php if($idStat=="001"){ $status = "active"; } else { $status = "no-active"; }?><li class="<?php echo $status; ?>" >
									<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/"><?php $plxShow->lang('HOME') ?></a>
								</li>
								<?php if($idStat=="002"){ $status = "active"; } else { $status = "no-active"; }?><li class="<?php echo $status; ?>" >
									<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static2/wiki"><?php $plxShow->lang('WIKI') ?></a>
								</li>

								<?php 
								//if($lang==="fr"){
									$plxShow->pageBlog('<li class="#page_class #page_status" id="#page_id"><a href="#page_url" title="#page_name">#page_name</a></li>'); 
								//}
								?>

								<?php if($idStat=="003"){ $status = "active"; } else { $status = "no-active"; }?><li class="<?php echo $status; ?>" >
									<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static3/about"><?php $plxShow->lang('ABOUT') ?></a>
								</li>
							</ul>
						</div>

					</nav>
				
				</div>

			</div>

		</div>

	</header>
	

	<div class="bg"></div>
