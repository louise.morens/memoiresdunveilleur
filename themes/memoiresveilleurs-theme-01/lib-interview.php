<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/characters/interviews";
$search = glob("".$folder."/*.json");
if($_GET["detail"]){
    $detailjson = $_GET["detail"];
} else{
    $detailjson = "no";
}
if($detailjson === 'no'){
    if (!empty($search)){ 
        asort($search);
        foreach ($search as $jsonpath) {
            
            $iditem = basename($jsonpath,'.json');
            $contents = file_get_contents($jsonpath);

            $get = json_decode($contents); 
            echo '<div class="wiki-item">';            
            $name = $get->{'title'}->{$lang};
            echo '<a href="/'.$lang.'/static11/charactersinterview?detail='.$iditem.'">';           
            echo $name.'</a>';  
        } 
    }
} else{
    $pagedetailjson = $folder.'/'.$detailjson.'.json';
    $contentDetailed = file_get_contents($pagedetailjson);
    $detail = json_decode($contentDetailed); 
    echo '<div class="wiki-item" id="int-'.$detailjson.'">';        
    $name = $detail->{'title'}->{$lang};     
    echo '<h3 class="wiki-title-item">'.$name.'</h3>';  
    echo'<p style="text-align:center"><strong>';
    echo $plxShow->lang('IDATE');
    echo '</strong> : <em>';
    if($lang==="fr"){
        $datei = $detail->{'date'}->{'fr'}.' '.$detail->{'date'}->{'year'};
    }else{
        $datei = $detail->{'date'}->{'year'}.','.$detail->{'date'}->{'en'};
    }
    echo $datei.'</em><br /><strong>';
    echo $plxShow->lang('ISCOPE');
    echo '</strong> : <em>'.$detail->{'scope'}->{$lang}.'</em><br /><strong>';
    echo $plxShow->lang('IPLACE');
    echo '</strong> : <em>'.$detail->{'place'}->{$lang}.'</em><br /><strong>';
    echo $plxShow->lang('IREPORTER');
    echo '</strong> : <em>'.$detail->{'reporter'}->{'name'}.' - '.$detail->{'reporter'}->{'job'}->{$lang}.'</em><br />';
    echo '<br/><div class="separation"></div><br/>';
    foreach($detail->{'interview'} as $qa){
        echo '<h2 class="question"><strong>'.$qa->{'question'}->{$lang}.'</strong></h2>';
        if($qa->{'answer'}){
            echo '<p class="answer ">'.$qa->{'answer'}->{$lang}.'</p>';
        } else if($qa->{'answerext'}){
            $pageDisplay = $folder.'/content/'.$qa->{'answerext'}->{$lang};
            $datahtml = file_get_contents($pageDisplay);
            $txt = html_entity_decode($datahtml);
            echo '<p class="answer ">'.$txt.'</p>';
        }
    }
}
?>