<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/general/";
$jsonpath = $folder."progress.json";
$contents = file_get_contents($jsonpath);

$get = json_decode($contents); 
$progressall = $get->{'progress'};
asort($progressall);

echo '<h3>';
$plxShow->lang('IN_PROGRESS');
foreach($progressall as $prog){    
    echo '</h3><div id="progress-'.$prog->{'id'}.'">';
    if($prog->{'type'}==="novel"){
        echo '<b>';
        $plxShow->lang('NOVEL-PROGRESS');
        echo '</b>';
    } else if ($prog->{'type'}==="visualnovel"){
        echo '<b>';
        $plxShow->lang('VN-PROGRESS');
        echo '</b>';
    }
    echo '<p>'.$prog->{'title'}->{$lang}.'</p></div>';
    echo'<progress value="'.$prog->{'progress'}.'" min="0" max="100">'.$prog->{'progress'}.'%</progress>';
    
}
?>