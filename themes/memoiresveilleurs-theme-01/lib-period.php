<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/periods";
$search = glob("".$folder."/*.json");

if (!empty($search)){ 
    asort($search);
    foreach ($search as $jsonpath) {
        $iditem = basename($jsonpath,'.json');
        $contents = file_get_contents($jsonpath);

        $data = json_decode($contents); 
        $get = $data->{'period'};
        echo '<div class="wiki-item" id="per-'.$iditem.'">';
        
        $name = $get->{'name'}->{$lang};
           
        
        echo '<h3 class="wiki-title-item">'.$name.'</h3>';  
        echo '<p class="em">'.$get->{'desc'}->{$lang}.'</p>';
               
        echo '<div class="wiki-item-detail"><ul class="no-style-liste">';
        $novels = $get->{'novels'};
        //asort($novels);
        foreach($novels as $nov){
            $translate = $nov->{'translate'};
            echo '<li class="wiki-period-li">';
            echo '<div class="period-title">'.$nov->{'name'}->{$lang};
            echo ' ('.$nov->{'dateperiod'}.')';
            
            echo '</div>'; 
            echo '<div class="period-link">';
            if($nov->{'refpage'} !=="" ){
                $refpage = $nov->{'refpage'};
                echo '<p><a href="/'.$lang.'/static14/publi?novel='.$refpage.'&type=official">';
                echo $plxShow->lang('DETAILED_PAGE');
                echo '</a></p>';
            }
            if($nov->{'freeread'} === "1" ){
                echo $plxShow->lang('FREEREAD_PAGE');
                if($translate === "1"){
                    echo ' (français/english)';
                } else{
                    echo ' (français)';
                }
                echo '</a></p></div>';
            }                                     
            echo '</li>';     
        }
        echo '</ul></div>';
        
        echo '</div>';
        //echo '<br/><div class="separation"></div><br/>';
    } 
} 
?>