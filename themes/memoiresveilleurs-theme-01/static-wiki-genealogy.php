<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

    <div class="container">

        <div class="grid">
            <div class="menu-wiki-bg col sml-12 med-3">

                <ul class="no-style-liste menu-wiki-ul">
                    <?php include(dirname(__FILE__).'/menu-wiki.php'); ?>
                </ul>

            </div>
            <div class="wiki-page col sml-12 med-9">

                <article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

                   
                    <div class="wiki-content">
                        <?php 
                            $pageToDisplay = "genealogy";
							include(dirname(__FILE__).'/lib-galery.php');
						?>
                    </div>
                </article>

            </div>



        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>