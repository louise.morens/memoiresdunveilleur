<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

    <div class="container">

        <div class="grid">
            <div class="menu-wiki-bg col sml-12 med-3">

                <ul class="no-style-liste menu-wiki-ul">
                    <?php include(dirname(__FILE__).'/menu-wiki.php'); ?>
                </ul>

            </div>
            <div class="wiki-page col sml-12 med-9">

                <article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

                    <header>
                        <h2 class="wiki-title-page">
                            <?php $plxShow->staticTitle(); ?>
                        </h2>
                    </header>
                    <div class="wiki-content">
                        <?php                             
								include(dirname(__FILE__).'/lib-interview.php');
							?>
                    </div>
                </article>

            </div>



        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>