<?php

$LANG = array(

#header.php
'MENU'					=> 'Menu',
'HOME'					=> 'Home',
'GOTO_CONTENT'			=> 'Goto content',
'GOTO_MENU'				=> 'Goto menu',
'COMMENTS_RSS_FEEDS'	=> 'Rss feeds comments',
'COMMENTS'				=> 'Comments',
'ARTICLES_RSS_FEEDS'	=> 'Rss feeds articles',
'ARTICLES'				=> 'Articles',

# sidebar.php
'CATEGORIES' 			=> 'Categories',
'LATEST_ARTICLES'		=> 'Latest articles',
'LATEST_COMMENTS'		=> 'Latest comments',
'ARCHIVES'				=> 'Archives',

# footer.php
'POWERED_BY'			=> 'Powered by',
'PLUXML_DESCRIPTION'	=> 'Blog or Cms without database',
'IN'					=> 'in',
'ADMINISTRATION'		=> 'Administration',
'GOTO_TOP'				=> 'Goto top',
'TOP'					=> 'Top',
'TRANSLATE'             => 'Translate by Louise Morens',
'TERMOFUSE'             => 'Terms of services and privacy',

# erreur.php
'ERROR'					=> 'Error found',
'BACKTO_HOME'			=> 'Goto home',

# common
'WRITTEN_BY'			=> 'Written by',
'CLASSIFIED_IN'			=> 'Classified in',
'TAGS'					=> 'Tags',

# commentaires.php
'SAID'					=> 'said',
'WRITE_A_COMMENT'		=> 'Write a comment',
'NAME'					=> 'Name',
'WEBSITE'				=> 'Site (optional)',
'EMAIL'					=> 'E-mail (optional)',
'COMMENT'				=> 'Comment',
'CLEAR'					=> 'Clear',
'SEND'					=> 'Send',
'COMMENTS_CLOSED'		=> 'Comments are closed',
'ANTISPAM_WARNING'		=> 'Anti-spam checking',

'REPLY'					=> 'Reply',
'REPLY_TO'				=> 'Reply to',
'CANCEL'				=> 'Cancel',

#AJOUT LM
'FOLLOW_ME'				=> 'Follow me',
'LANG_EN'				=> 'english',
'LANG_FR'				=> 'french',
'NOVEL'					=> 'Novels',
'NOVEL_PUB'				=> 'Publication',
'NOVEL_LIB'				=> 'Free read',
'ABOUT'					=> 'About',
'IN_PROGRESS'           => 'Actual project',
'ILLUS_LIB'             => 'Artworks',
'THANKS'                => 'Thanks',
'OTHER'                 => 'Other',
'MUSIC_LIB'             => 'Musical universe',
'RELATIVE_NOVEL'        => 'Novel or short story associate: ',
'VIDEO_MUSIC_LIB'       => 'Videos associated with musical themes.',
'TIPEEE'                => 'Support Memoirs of a Watchman on Tipeee',
#Wiki menu
'PLANET'            => 'Planets',
'ORDERWATCHMEN'     => 'Order of Watchmen',
'ORDERVAL'          => 'Order of Valaquenta',
'CHARACTERS'        => 'Characters',
'CHARACTERSINTERVIEW'=>'Characters interviews',
'ETYMOLOGY'         => 'Ethymology',
'GENEALOGY'         => 'Genealogy',
'PERIOD'            => 'Period',
'ARTWORKS'          => 'Artworks',

#Wiki data
'WIKI_DESC'         => 'Memoirs of Watchmen contain several volumes that tell the story of Watchman Jack Cooper, his companions and their more or less close descendants.',
'PEOPLE'            => 'People',
'PLANET_BIRTH'      => 'Birth planet',
'DATE_BIRTH'        => 'Birthday',
'DATE_DEATH'        => 'Deathday',
'AGEDIED'           => 'Age when died',
'DETAILED_PAGE'     => 'See detailed',
'FREEREAD_PAGE'     => 'Read for free available',
'SOUND_THEME'       => 'Associated musical theme',
'SOUND_THEME_CHAR'  => 'Musical theme associated with the characters: ',
'SOUND_THEME_PLANET' => 'Musical theme associated with the planet: ',

#publi
'RESUME'            => 'Resume',
'EXTRACT'           => 'Extract',
'COVER'             => 'Cover',
'PAPER'             => 'Paper edition is available on the publisher\'s website',
'EBOOK'             => 'Ebook edition is available on the the publisher\'s website',
'FREEREAD'          => 'Available for free read',
'EDITOR'            => 'Publish by',
'PUBLISH'           => 'date of publication',
'SWITCHLANG'        => 'Read the french version',
#interviews
'INTERVIEWTITLE'    => 'Read the interviews of the characters',
'IDATE'             => 'Date',
'ISCOPE'            => 'Scope',
'IPLACE'            => 'Place',
'IREPORTER'         => 'Reporter',
#Progress
'NOVEL-PROGRESS'    => 'Writing',
'VN-PROGRESS'       => 'Visual novel/game',
#Visual Novel
'TITLE_PAGE'        => 'Visual Novels',
'PUBLISH_DATE'      => 'Publish date',
);

?>
