<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/general/";
$jsonpath = $folder.$pageToDisplay.".json";
$contents = file_get_contents($jsonpath);

$get = json_decode($contents); 
echo '<div class="wiki-item" id="gen-'.$pageToDisplay.'">';
$words = $get->{'words'};
asort($words);
echo '<div class="wiki-item-detail"><dl>';
foreach($words as $wd){
    echo '<dt class="wiki-dt">';
    echo $wd->{'word'}->{$lang};
    echo '</dt>';
    echo '<dd class="wiki-dd">'.$wd->{'desc'}->{$lang}.'</dd>';
}
echo '</dl></div></div>';
?>