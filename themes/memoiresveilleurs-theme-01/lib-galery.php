<?php if(!defined('PLX_ROOT')) exit; 

$folder = "wiki_pages/artworks/";
$jsonpath = $folder.$pageToDisplay.".json";
$contents = file_get_contents($jsonpath);
$get = json_decode($contents); 
$galery = $get->{'galery'};
$galleryId = $galery->{'name'};
$galerieTitle = $galery->{'desc'}->{$lang};
$galerieUrl = 'data/medias/'.$galery->{'uri'};
//echo $galerieUrl;
$illu = $galery->{'artwork'};

echo '<div class="center title-gallery" id="'.$galleryId.'">'.$galerieTitle.'</div>';
echo '<div class="col-3">';
    foreach($illu as $galImg) {
        $img = $galImg->{'img'};
       // echo $img;
        $urlimg = $galerieUrl.'/'.$img;
        //echo $urlimg;
        $thumbimg = $galerieUrl.'/'.$galImg->{'thumb'};
        $altimg = $galImg->{'alt'}->{$lang};
        $captionimg = $galImg->{'caption'}->{$lang};
        echo '<figure class="figure flex-gallery-mv">';
        echo'<a href="'.$urlimg.'">';
        echo'<img class="figure-img img-fluid rounded" src="'.$thumbimg.'" alt="'.$alt.'" title="'.$alt.'">';
        echo'</a>';
        echo'<figcaption class="figure-caption">'.$captionimg.'</figcaption>';

        echo'</figure>';
    }
    echo '</div>';


