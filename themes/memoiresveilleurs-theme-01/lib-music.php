<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/general/";
$jsonpath = $folder."music.json";
$contents = file_get_contents($jsonpath);

$get = json_decode($contents); 
$soundall = $get->{'sound'};
$videoall = $get->{'video'};



foreach($soundall as $sound){    
    echo '<div id="sound-'.$sound->{'id'}.'">';
    if($sound->{'type'}==="bandcamp"){
        if($sound->{'relative'}->{'type'} === "characters"){
            echo '<b>';
            $plxShow->lang('SOUND_THEME_CHAR');
            echo '</b><br/>';
            echo $sound->{'relative'}->{'name'}->{$lang};
        }
        else if ($sound->{'relative'}->{'type'}==="planet"){
            echo '<b>';
            $plxShow->lang('SOUND_THEME_PLANET');
            echo '</b><br/>';
            echo '<b>'.$sound->{'relative'}->{'name'}->{$lang}.'</b>';
        }

        echo '<iframe style="border: 0; width: 100%; height: 42px;" src="'.$sound->{'urlsound'} .' seamless><a href="'.$sound->{'urlalbum'}.'">'.$sound->{'title'} .'</a></iframe>';
    }
    echo '<div class="separation"></div>';
    echo '</div>';
}

echo '<h3>';
$plxShow->lang('VIDEO_MUSIC_LIB');
echo '</h3>';

foreach($videoall as $video){    
    echo '<div id="video-'.$video->{'id'}.'">';

    if($video->{'type'}==="youtube"){
        if($video->{'relative'}->{'type'} === "character"){
            echo '<b>'.$video->{'title'}.'</b>';
        }
        else if ($video->{'relative'}->{'type'}==="novel"){
            echo '<b>';
            $plxShow->lang('RELATIVE_NOVEL');
            echo $video->{'relative'}->{'name'}->{$lang}.'</b>';
            echo '<br/>';
            echo $video->{'title'};

        }
        echo '<iframe src="'.$video->{'urlsound'}.'" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ';
        echo 'allowfullscreen="" width="650" height="366" frameborder="0"></iframe>';
    }
    echo '<div class="separation"></div>';

    echo '</div>';

}
?>