<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

    <div class="container">

        <div class="grid">

            <div class="content col sml-12 med-9">

                <article class="article" id="post-<?php echo $plxShow->artId(); ?>">

                    <header>                       
                        <h2>
                            <?php $plxShow->artTitle(); ?>
                        </h2>
                        
                    </header>
                    <?php $plxShow->artThumbnail(); ?>
                    <?php $plxShow->artContent(); ?>

                </article>
                

            </div>

            <?php include(dirname(__FILE__).'/sidebar.php'); ?>

        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>