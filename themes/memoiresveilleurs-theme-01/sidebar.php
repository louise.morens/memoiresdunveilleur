<?php if(!defined('PLX_ROOT')) exit; ?>

	<aside class="aside col sml-12 med-3">
		<!--<h3>
			<?php $plxShow->lang('IN_PROGRESS'); ?>
		</h3>
		<div id="progress">				
			<progress value="25" min="0" max="100">25%</progress>
			<p>Métanélie</p>
		</div>-->
		<?php include(dirname(__FILE__).'/lib-progressbar.php'); ?>

		<!--h3>
			<?php $plxShow->lang('CATEGORIES'); ?>
		</h3>

		<ul class="cat-list unstyled-list">
			<?php $plxShow->catList('','<li id="#cat_id"><a class="#cat_status" href="#cat_url" title="#cat_name">#cat_name</a> (#art_nb)</li>'); ?>
		</ul-->
		<?php // if($lang==="fr"){ ?>
								
		<h3>
			<?php $plxShow->lang('LATEST_ARTICLES'); ?>
		</h3>

		<ul class="lastart-list unstyled-list">
			<?php $plxShow->lastArtList('<li><a class="#art_status" href="#art_url" title="#art_title">#art_title</a></li>'); ?>
		</ul>
		<?php //} ?>
		<h3>
			<?php $plxShow->lang('NOVEL'); ?>
		</h3>

		<ul class="list">
			<li><a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static14/publi?type=official" title="<?php $plxShow->lang('NOVEL_PUB'); ?>"><?php $plxShow->lang('NOVEL_PUB'); ?></a></li>
			<li><a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static14/publi?type=freeread" title="<?php $plxShow->lang('NOVEL_LIB'); ?>"><?php $plxShow->lang('NOVEL_LIB'); ?></a></li>
			<li><a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static10/illustrations" title="<?php $plxShow->lang('ILLUS_LIB'); ?>"><?php $plxShow->lang('ILLUS_LIB'); ?></a></li>
			<li>
			<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static16/music" title="<?php $plxShow->lang('MUSIC_LIB'); ?>"><?php $plxShow->lang('MUSIC_LIB'); ?></a></li>	
			<li>
			<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static18/game" title="<?php $plxShow->lang('TITLE_PAGE'); ?>"><?php $plxShow->lang('TITLE_PAGE'); ?></a></li>	
		</ul>
		<h3>
			<?php $plxShow->lang('OTHER'); ?>
		</h3>

		<ul class="list">
		<li>	<a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static17/newsletter">Newsletter</a>
		</li>
		<li>	<a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static15/patron"><?php $plxShow->lang('THANKS') ?></a>
		</li>
		
		</ul>
		<?php $plxShow->lang('TIPEEE') ?>
		<a href="https://fr.tipeee.com/memoires-dun-veilleur" class="tipeee-project-btn"><?php $plxShow->lang('TIPEEE') ?></a>
		<script async src="https://plugin.tipeee.com/widget.js" charset="utf-8"></script>

		<h3>
			<?php $plxShow->lang('TAGS'); ?>
		</h3>

		<ul class="tag-list">
			<?php $plxShow->tagList('<li class="tag #tag_size"><a class="#tag_status" href="#tag_url" title="#tag_name">#tag_name</a></li>', 20); ?>
		</ul>
		
		
		<!--h3>
			<?php $plxShow->lang('LATEST_COMMENTS'); ?>
		</h3>

		<ul class="lastcom-list unstyled-list">
			<?php $plxShow->lastComList('<li><a href="#com_url">#com_author '.$plxShow->getLang('SAID').' : #com_content(34)</a></li>'); ?>
		</ul>

		<h3>
			<?php $plxShow->lang('ARCHIVES'); ?>
		</h3>

		<ul class="arch-list unstyled-list">
			<?php $plxShow->archList('<li id="#archives_id"><a class="#archives_status" href="#archives_url" title="#archives_name">#archives_name</a> (#archives_nbart)</li>'); ?>
		</ul-->

	</aside>
