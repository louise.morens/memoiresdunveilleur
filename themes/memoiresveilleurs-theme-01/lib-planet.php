<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/planets";
$search = glob("".$folder."/*.json");
echo '<figure class="wiki-figure">';
echo '<a href="/data/medias/screenshots/univers.png"><img src="/data/medias/screenshots/univers.tb.png" class="wiki-img" alt="Schema"/></a>';
echo '<figcaption class="wiki-caption">Schema</figcaption>';
echo '</figure>';
if (!empty($search)){ 
    foreach ($search as $jsonpath) {
        $iditem = basename($jsonpath,'.json');
        $contents = file_get_contents($jsonpath);

        $get = json_decode($contents); 
        echo '<div class="wiki-item" id="char-'.$iditem.'">';
        
        $name = $get->{'name'}->{$lang};
           
        
        echo '<h3 class="wiki-title-item">'.$name.'</h3>';  
        echo '<p>'.$get->{'desc'}->{$lang}.'</p>';
       
        if(!empty($get->{'people'})){
            echo '<div class="wiki-item-detail"><dl>';
            $people = $get->{'people'};
            asort($people);
            foreach($people as $peuple){
                echo '<dt class="wiki-dt">';
                echo $peuple->{'name'}->{$lang};
                echo '</dt>';
                if(!empty($peuple->{'characters'})){
                    $characters = $peuple->{'characters'};
                    asort($characters);
                    echo '<dd class="wiki-dd"><ul class="no-style-liste">';
                    foreach($characters as $pers){
                        $namechar = $pers->{'name'};
                        if($pers->{'link'} !==""){
                            $linkpers = $pers->{'link'};

                            $anch = '<a href="/'.$lang.'/static6/characters/#'.$linkpers.'">'.$namechar.'</a>';                            
                        } else {
                            $anch =  $namechar;
                        }
                        echo '<li>'.$anch.'</li>';
                    }
                    echo '</ul></dd>';
                }                
            }
            echo '</dl></div>';
        }
        echo '</div>';
        //echo '<br/><div class="separation"></div><br/>';
    } 
} 
?>