<p>Welcome to my world. These pages are dedicated to "Mémoires d'un Veilleur" (Memoirs of a Watchman). </p>
<p>In this SciFi universe, you could find many references in several human mythologies. The story begun on Earth, in the
    first volume, "L'Année perdue" (The Lost Year), which was published by Atramenta Editions, in 2018, July. The second
    volume ‘Trahisons’ (treason) was published in 2019, February, always by Atramenta Editions. It takes place on Earth
    and on a space station. </p>
<p>The following volumes take place on Earth, and on the other planets described in this universe. Some short stories
    unfolds on the Earth before the Lost Year. They allow you to get to know each character better: Jack Cooper, Solène
    Andersen, Alexandre Bridger, Baptiste Le Bel, Charlie Dubois. They can find them, in ‘Coopération et vérité’
    (Cooperation and Truth) and ‘Rencontres’ (Meetings). Several interviews realised after the second volumes also get a
    lot of information about them.</p>
<p align="center"><img src="data/medias/illustrations/insignes/insigne-gen.tb.jpg" title="insigne-gen.tb.jpg"
        alt="insigne-gen.jpg" /></p>
<p>This image represents the precepts that every watchman must respect: Respect, Protection, Justice and Safeguard. You
    can learn more about this universe in the wiki. You will find information not only about the Order of the Watchmen,
    but also about characters, planets, peoples, etc. </p>
<p>The Memoirs of a watchman include the adventures of the Watchman Jack Cooper and his companions: Vlad, Joshua,
    Alexandre, Solène, Baptiste, Charlie, etc., as well as those of their descendants. I will update the genealogies,
    and chronologies as novels become available. Several illustrations become, I’m not graphic artist and I just try to
    illustrate my world. </p> <br />
<div class="separation"></div>
<br />
<p>The blog includes articles that describe the characters, the universe, the tools used to write, etc. It exists only
    since the beginning of 2018. I will add articles regularly, don’t hesitate to consult it and to leave your comments. It is only available in french.
</p> <br />
<div class="separation"></div>
<br /> <strong>Return of reading (in french) </strong>&nbsp; <p class="western">L'Année perdue : Les chroniques de Lee
    Ham<a
        href="https://litteratutemltipleunerichesse.wordpress.com/2019/01/13/lannee-perdue-memoires-dun-veilleur-louise-morens-2018"
        target="_blank"> </a><a
        href="https://litteratutemltipleunerichesse.wordpress.com/2019/01/13/lannee-perdue-memoires-dun-veilleur-louise-morens-2018?fbclid=IwAR0YRzsqtjPpheYDYd6H0W2lGdCKE2cFo73KYHSs9Y2A5PQASdlumhcSARk"
        target="_blank">https://litteratutemltipleunerichesse.wordpress.com/2019/01/13/lannee-perdue-memoires-dun-veilleur-louise-morens-2018</a>
</p> <br />
<div class="separation"></div>
<br /> <strong>Interviews (in french) </strong>
<p style="margin-bottom: 0cm"><a href="https://mambiehlblog.wordpress.com/2019/01/24/flash-auteur-15-louise-morens/"
        target="_blank">Les chroniques pressées</a> : interview <a
        href="https://chroniquespressees.com/2019/01/24/flash-auteur-15-louise-morens/" target="_blank">Flash auteur</a>
</p>
<p>Interview 2 par <a href="http://lecturesfamiliales.wordpress.com/" target="_blank">Les lectures familiales</a> : <a
        href="https://lecturesfamiliales.wordpress.com/2019/03/12/jeu-auteur-mystere-27/" target="_blank">jeu de
        l'auteur mystère</a> et la <a
        href="https://lecturesfamiliales.wordpress.com/2019/03/16/jeu-auteur-mystere-27bis/?fbclid=IwAR0ZoaSKkBOcAsxuJh611VkkbzGZYnwXOS0jNPYKIH-DvHU05A9f6DZGa8g"
        target="_blank">réponse au jeu</a> </p> &nbsp;