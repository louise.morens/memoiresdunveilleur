# Memoirs of a Watchman website - Mémoires d'un Veilleur site web

## Description

![alt tag](themes/memoiresveilleurs-theme-01/preview.png)

This repository is the full website (with no data - only data templates not to take up too much space) of the website dedicated to [the memoirs of a Watchman](https://www.louisemorens.com). I must add information on the wiki about people of every planet. The work is in progress and will be commited when I will be decided about the data to display.

The website is powered by a [PluXml](http://www.pluxml.org/) open-source ([GNU GPL V3](http://www.gnu.org/copyleft/gpl.html)) CMS and several plugins. The theme is built around the default theme. I added several files to parse the json files used to describe informations in the wiki (planets, characters, period of the cycle, novels and shorts stories, statics pages with specific informations, etc). The templates of the json files are avalaible in [wiki_pages/templates](wiki_pages/templates). Specifics directories are available in [wiki_pages](wiki_pages).

The themes files used to parse them are named static-xxx.php and call lib-xxx.php.

I added several informations in the header file and footer file. The wiki have a specific menu which is generated with a php file.

The "core", "data", 'plugins" folders are untouched. I use pure PluXml to allow easier upgrade.
I must clean the php code, yet. I will do this when I will be sure that I haven't touch the code before clean it.



